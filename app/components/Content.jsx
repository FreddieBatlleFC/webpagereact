import React from 'react';
import Slideshow from './Slideshow';
import Logo from './Logo';
import Burger from './Burger';
import Footer from './Footer';

const divStyle = {
  transform: 'translateY(0px) translateZ(0px)',
};

const urls = [
  'https://prismic-io.s3.amazonaws.com/afmusic/5072cbfe750a3683586dcbffb5d44f04e39b260d_inuit_fond.jpg',
  'https://prismic-io.s3.amazonaws.com/afmusic/88aac9db67690a126e8b1805739d05b6fb869db6_camille_fond.jpg',
  'https://prismic-io.s3.amazonaws.com/afmusic/c79e11713b3b6f37ba7f05b194d8a2bd1b61329c_cigarette_fond.jpg',
  'https://prismic-io.s3.amazonaws.com/afmusic/88aac9db67690a126e8b1805739d05b6fb869db6_camille_fond.jpg',

];

const albumUrls = [
  'https://prismic-io.s3.amazonaws.com/afmusic/cb9e28c2140edfe8cbbc46ac2d36dd0d8961a388_inu__it_-_tomboy_480.jpg',
  'https://prismic-io.s3.amazonaws.com/afmusic/090949470a67c496689e2ffe24a80452d984b395_bonobo---bambro-koyo-ganda.jpg',
  'https://prismic-io.s3.amazonaws.com/afmusic/eba3e6ddbb4959cd54700106f8eeb10adca90524_asgeir-stardust.jpg',
  'https://prismic-io.s3.amazonaws.com/afmusic/4da69fe183530ea323a58c30613934fa9e14adda_camille-seeds.jpg',
];

const albumTitle = [
  'Inüit',
  'Bonobo',
  'Asgeir',
  'Camille',
];

const songTitle = [
  'Hush',
  'Bambro Koyo Ganda (feat Innov Gnawa)',
  'Stardust',
  'Seeds',
];


const Content = () =>

  (
    <div className="js-content u-pos-tl u-fit-w u-overflow-h u-bg--white u-fixed" style={divStyle}>
      <Slideshow
        imageUrls={urls}
        albumImageUrls={albumUrls}
        albumTitle={albumTitle}
        songTitle={songTitle}
      />
      <Logo />
      <Burger />
      <Footer />
    </div>
  );

export default Content;

