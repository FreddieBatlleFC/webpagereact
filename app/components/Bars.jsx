import React from 'react';


const divStyle = {
  backgroundColor: 'rgba(26, 95, 133, 0.01)',
};

const Bars = () =>
  (
    <div className="c-volume-nav u-absolute u-pos-bl
  u-fit-w u-flex u-justify-content-c u-marg-b-xl | u-marg-b-xxl@md"
    >
      <div className="c-volume-nav-line-container" style={divStyle}>
        <div className="c-volume-nav-line u-relative
      u-bg--red is-next"
        >
          <span className="c-volume-nav-line-number u-absolute
        u-pos-bl t-text--xxs u-color--white u-block"
          >01
          </span>
        </div>
      </div>
      <div className="c-volume-nav-line-container" style={divStyle}>
        <div className="c-volume-nav-line u-relative is-active">
          <span className="c-volume-nav-line-number
        u-absolute u-pos-bl t-text--xxs u-color--white u-block"
          >02
          </span>
        </div>
      </div>
      <div className="c-volume-nav-line-container" style={divStyle}>
        <div className="c-volume-nav-line u-relative is-prev">
          <span className="c-volume-nav-line-number
            u-absolute u-pos-bl t-text--xxs u-color--white u-block"
          >03
          </span>
        </div>
      </div>
      <div className="c-volume-nav-line-container" style={divStyle}>
        <div className="c-volume-nav-line u-relative u-bg--red is-prev-2">
          <span className="c-volume-nav-line-number
        u-absolute u-pos-bl t-text--xxs u-color--white u-block"
          >04
          </span>
        </div>
      </div>
      <div className="c-volume-nav-line-container" style={divStyle}>
        <div className="c-volume-nav-line u-relative is-prev-3">
          <span className="c-volume-nav-line-number
        u-absolute u-pos-bl t-text--xxs u-color--white u-block"
          >05
          </span>
        </div>
      </div>
      <div className="c-volume-nav-line-container" style={divStyle}>
        <div className="c-volume-nav-line u-relative u-bg--red">
          <span className="c-volume-nav-line-number
        u-absolute u-pos-bl t-text--xxs u-color--white u-block"
          >06
          </span>
        </div>
      </div>
    </div>
  );

export default Bars;
