import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

let divStyle = {
  backgroundColor: 'rgba(26, 95, 133, 0.01)',
  transform: 'translateX(0px) translateZ(0px)',
};

const infoDivStyle = {
  backgroundColor: 'rgba(26, 95, 133, 0.01)',
};

const trackStyle = {
  color: 'rgb(88, 211, 225)',
};

const lineStyle = {
  background: 'rgb(88, 211, 225) none repeat scroll 0% 0%',
};

const circle1Style = {
  backgroundColor: 'rgb(228, 119, 92)',
};

const circle2Style = {
  backgroundColor: 'rgb(235, 205, 194)',

};
const circle3Style = {
  backgroundColor: 'rgb(31, 102, 117)',

};

class AlbumImgs extends React.Component {
  render() {
    const keys = this.props.keys + 1;
    const classes = classNames('c-track-item u-w4of12 u-force-3d u-inline-block\n' +
        '      u-white-space-normal | u-w5of12@lg u-w10of12@md u-w5of6@sm js-elem\n' +
        '      c-track-item--0', {
      'is-active': this.props.isHovered,
    });
    if (!this.props.isHovered && this.props.isHover !== 0) {
      if (this.props.isHover > keys) {
        divStyle = {
          backgroundColor: 'rgba(26, 95, 133, 0.01)',
          transform: 'translateX(-160px) translateZ(0px)',
        };
      } else {
        divStyle = {
          backgroundColor: 'rgba(26, 95, 133, 0.01)',
          transform: 'translateX(160px) translateZ(0px)',
        };
      }
    } else {
      divStyle = {
        backgroundColor: 'rgba(26, 95, 133, 0.01)',
        transform: 'translateX(0px) translateZ(0px)',
      };
    }
    return (
      <div
        className={classes}
        style={divStyle}
        onMouseOver={() => this.props.hoverHandler(this.props.keys)}
        onFocus={() => this.props.hoverHandler(this.props.keys)}
        onMouseOut={() => this.props.mouseOutHandler(this.props.keys)}
        onBlur={() => this.props.mouseOutHandler(this.props.keys)}
      >
        <a
          href="//"
          className="c-track-item-inner
        u-relative u-block"
          draggable="false"
        >
          <span className="c-track-item-number t-text u-bold
          u-absolute u-offset-l-w1of8"
          >0{keys}
          </span>
          <div className="c-track-item-cover u-relative u-w5of8 u-offset-l-w3of8 u-pad-x-md">
            <div
              r="-10,10,0"
              className="js-parent
            c-track-item-cover-inner u-fit u-block u-relative"
            >
              <div className="c-track-item-cover-vinyl u-absolute u-pos-tl u-fit u-force-3d" />
              <div className="c-track-item-cover-img
              u-absolute u-pos-tl u-fit  u-force-3d u-overflow-h"
              >
                <img
                  src={this.props.albumImageUrl}
                  draggable="false"
                  className="u-absolute u-pos-tl u-fit u-force-3d"
                  alt="album"
                />
              </div>
            </div>
            {keys === 1 &&
              <div className="c-track-item-icon u-absolute u-pos-tr u-flex
               u-align-items-c u-justify-content-c"
              >
                <svg
                  version="1.1"
                  xmlns="http://www.w3.org/2000/svg"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
                  x="0px"
                  y="0px"
                  viewBox="0 0 11 17"
                  xmlSpace="preserve"
                  width="11"
                  height="17"
                  className="u-fill--white"
                >
                  <path d="M10.9,8.5c0-0.4-0.3-0.7-0.7-0.7S9.6,
                  8.1,9.6,8.5c0,2.2-1.8,4.1-4.1,4.1s-4.1-1.8-4.1-4.1c0-0.4-0.3-0.7-0.7-0.7
                              S0.1,8.1,0.1,8.5c0,2.8,2.1,5.1,
                              4.8,5.4v1.7h-1c-0.4,0-0.7,0.3-0.7,0.7S3.4,
                              17,3.8,17h3.4c0.4,0,0.7-0.3,0.7-0.7s-0.3-0.7-0.7-0.7
                              h-1v-1.7C8.9,13.6,10.9,11.3,10.9,8.5z"
                  />
                  <path d="M5.5,11.6c1.7,0,3.1-1.4,3.1-3.1V3.1C8.6,
                  1.4,7.2,0,5.5,0S2.4,1.4,2.4,3.1v5.4C2.4,10.2,3.8,
                  11.6,5.5,11.6z M3.8,3.1
                              c0-0.9,0.8-1.7,1.7-1.7s1.7,0.8,1.7,
                              1.7v5.4c0,0.9-0.8,1.7-1.7,1.7S3.8,9.4,3.8,8.5V3.1z"
                  />
                </svg>
              </div>
          }

          </div>
          { ((keys === 1) || (keys === 4)) &&
            <div className="c-track-item-sticker u-offset-l-w3of8
           t-text--xs u-uppercase u-absolute u-pos-bl u-bold u-pointer-none"
            >
            Exclusive Interview
              <span className="c-track-item-sticker-inner
             u-absolute u-pos-tl"
              >Exclusive Interview
              </span>
            </div>
            }
          <div
            className="u-absolute u-fit-h u-pos-tl u-flex u-align-items-c
           u-w3of5 u-pointer-none u-force-3d"
            style={infoDivStyle}
          >
            <div>
              <h2 className="c-track-item-author t-h2 u-color--white">{this.props.albumTitle}</h2>
              <h3
                className="c-track-item-title t-text--xl u-medium u-marg-t-xs"
                style={trackStyle}
              >
                {this.props.songTitle}
              </h3>
              <span className="c-track-item-line c-line u-inline-block">
                <span
                  className="c-line-1"
                  style={lineStyle}
                />
                <span
                  className="c-line-2"
                  style={lineStyle}
                />
                <span
                  className="c-line-3"
                  style={lineStyle}
                />
              </span>
              <div className="u-marg-t-xs u-flex">
                <span
                  className="c-track-item-color"
                  style={circle1Style}
                />
                <span
                  className="c-track-item-color"
                  style={circle2Style}
                />
                <span
                  className="c-track-item-color"
                  style={circle3Style}
                />
              </div>
            </div>
          </div>
        </a>
      </div>
    );
  }
}


AlbumImgs.propTypes = {
  albumImageUrl: PropTypes.string.isRequired,
  keys: PropTypes.number.isRequired,
  songTitle: PropTypes.string.isRequired,
  albumTitle: PropTypes.string.isRequired,
  isHovered: PropTypes.bool.isRequired,
  isHover: PropTypes.number.isRequired,
  hoverHandler: PropTypes.func.isRequired,
  mouseOutHandler: PropTypes.func.isRequired,

};

export default AlbumImgs;

