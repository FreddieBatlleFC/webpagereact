import React from 'react';

const Footer = () => (
  <div className="c-footer u-absolute u-pos-bl u-marg-l-lg t-text--xs
    u-bold u-uppercase u-flex u-align-items-c | u-marg-l-md@md | u-hide@sm"
  >
    <a
      href="https://www.airfrance.com/indexCom_en.html"
      target="_blank"
      rel="noopener noreferrer"
      className="c-footer-btn t-btn--secondary u-marg-r-sm"
    >
      <span className="u-relative">VISIT AIRFRANCE.COM</span>
    </a>
    <span>
      <a href="/fr" className="c-footer-lang-link t-link--primary">FR</a>
            /
      <a href="/en" className="c-footer-lang-link t-link--primary is-active">EN</a>
    </span>
    <span className="u-marg-x-xs">•</span>
    <a href="mailto:mail.music@airfrance.fr" className="t-link--primary">contact</a>
    <span className="u-marg-x-xs">•</span>
    <a href="/en/credits" className="t-link--primary">Credits</a>
    <span className="u-marg-x-xs">•</span>
    <a href="/en/legal" className="t-link--primary">Legal</a>
  </div>
);

export default Footer;
