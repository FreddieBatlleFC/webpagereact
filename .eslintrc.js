module.exports = {
    "extends": "airbnb",
    "settings": {
        "import/resolver": {
            "webpack": "webpack.config.js"
        }
    },
    "env": {
        "browser": true,
        "node": true
    },
    "rules": {
        "class-methods-use-this": "warn",
        "react/prefer-stateless-function": "warn"
    }
};