import React from 'react';

const Burger = () => (
  <button className="c-burger t-btn u-absolute u-pos-tr u-text-center
   u-marg-t-lg u-marg-r-lg | u-marg-r-md@md | u-marg-t-sm@sm u-marg-r-sm@sm"
  >
    <div className="c-burger-inner u-relative u-inline-block">
      <span className="c-burger-line" />
      <span className="c-burger-line" />
      <span className="c-burger-line" />
    </div>
  </button>
);

export default Burger;
